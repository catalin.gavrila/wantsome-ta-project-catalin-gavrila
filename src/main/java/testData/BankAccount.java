package testData;

import java.util.Objects;

public class BankAccount {
    private String accountID;
    private String accountType;
    private String accountBalance;
    private String accountAvailable;

    public BankAccount() {
        accountType = "CHECKING";
    }

    public BankAccount(String accountID, String accountType, String accountBalance, String accountAvailable) {
        this.accountID = accountID;
        this.accountType = accountType;
        this.accountBalance = accountBalance;
        this.accountAvailable = accountAvailable;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(String accountBalance) {
        this.accountBalance = accountBalance;
    }

    public String getAccountAvailable() {
        return accountAvailable;
    }

    public void setAccountAvailable(String accountAvailable) {
        this.accountAvailable = accountAvailable;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "accountID='" + accountID + '\'' +
                ", accountType='" + accountType + '\'' +
                ", accountBalance='" + accountBalance + '\'' +
                ", accountAvailable='" + accountAvailable + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return accountID.equals(that.accountID) && accountBalance.equals(that.accountBalance) && accountAvailable.equals(that.accountAvailable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountID, accountType, accountBalance, accountAvailable);
    }
}
