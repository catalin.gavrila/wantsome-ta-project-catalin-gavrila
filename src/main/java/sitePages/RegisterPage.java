package sitePages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterPage {
    private WebDriver driver;

    private By firstNameInput = By.id("customer.firstName");
    private By lastNameInput = By.id("customer.lastName");
    private By addressInput = By.id("customer.address.street");
    private By cityInput = By.id("customer.address.city");
    private By stateInput = By.id("customer.address.state");
    private By zipCodeInput = By.id("customer.address.zipCode");
    private By phoneNumberInput = By.id("customer.phoneNumber");
    private By ssnInput = By.id("customer.ssn");
    private By usernameInput = By.id("customer.username");
    private By passwordInput = By.id("customer.password");
    private By confirmPasswordInput = By.id("repeatedPassword");
    private By registerButton = By.xpath("//*[@id=\"customerForm\"]/table/tbody/tr[13]/td[2]/input");

    private By loggedInTitle = By.cssSelector("h1[class=\"title\"]");
    private By registerSuccessMessage = By.xpath("//*[@id=\"rightPanel\"]/p");
    private By welcomeTitle = By.xpath("//*[@id=\"leftPanel\"]/p");

    //error elements
    private By firstNameError = By.id("customer.firstName.errors");
    private By lastNameError = By.id("customer.lastName.errors");
    private By addressError = By.id("customer.address.street.errors");
    private By cityError = By.id("customer.address.city.errors");
    private By stateError = By.id("customer.address.state.errors");
    private By zipCodeError = By.id("customer.address.zipCode.errors");
    //private By phoneNumberInput = By.id("customer.phoneNumber");
    private By ssnError = By.id("customer.ssn.errors");
    private By usernameError = By.id("customer.username.errors");
    private By passwordError = By.id("customer.password.errors");
    private By confirmPasswordError = By.id("repeatedPassword.errors");

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public By getLoggedInTitle() {
        return loggedInTitle;
    }

    public void fillFirstName(String firstName) {driver.findElement(firstNameInput).sendKeys(firstName);}
    public void fillLastName(String lastName) {driver.findElement(lastNameInput).sendKeys(lastName);}
    public void fillAddress(String address) {
        driver.findElement(addressInput).sendKeys(address);
    }
    public void fillCity(String city) {
        driver.findElement(cityInput).sendKeys(city);
    }
    public void fillState(String state) {
        driver.findElement(stateInput).sendKeys(state);
    }
    public void fillZipCode(String zipCode) {
        driver.findElement(zipCodeInput).sendKeys(zipCode);
    }
    public void fillPhoneNumber(String phoneNumber) {
        driver.findElement(phoneNumberInput).sendKeys(phoneNumber);
    }
    public void fillSsn(String ssn) {
        driver.findElement(ssnInput).sendKeys(ssn);
    }
    public void fillUsername(String username) {
        driver.findElement(usernameInput).sendKeys(username);
    }
    public void fillPassword(String password) {
        driver.findElement(passwordInput).sendKeys(password);
    }
    public void fillConfirmPass(String confirmPassword) {driver.findElement(confirmPasswordInput).sendKeys(confirmPassword);}

    public void clickRegister() {
        driver.findElement(registerButton).click();
    }

    public String getLoggedInTitleMessage() {
        return driver.findElement(loggedInTitle).getText();
    }

    public String getRegisterSuccessMessage() {
        return driver.findElement(registerSuccessMessage).getText();
    }

    public String getWelcomeMessage() {
        return driver.findElement(welcomeTitle).getText();
    }

    public String getFirstNameErrors() { return driver.findElement(firstNameError).getText();}
    public String getLastNameErrors() { return driver.findElement(lastNameError).getText();}
    public String getAddressErrors() { return driver.findElement(addressError).getText();}
    public String getCityErrors() { return driver.findElement(cityError).getText();}
    public String getStateErrors() { return driver.findElement(stateError).getText();}
    public String getZipCodeErrors() { return driver.findElement(zipCodeError).getText();}
    public String getSsnErrors() {return driver.findElement(ssnError).getText();}
    public String getUsernameErrors() {return driver.findElement(usernameError).getText();}
    public String getPasswordErrors() {return driver.findElement(passwordError).getText();}
    public String getConfirmPasswordErrors() {return driver.findElement(confirmPasswordError).getText();}
}
