package sitePages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    private WebDriver driver;
    private By usernameInput = By.name("username");
    private By passwordInput = By.name("password");
    private By loginBtn = By.xpath("//*[@id=\"loginPanel\"]/form/div[3]/input");
    private By loginError= By.cssSelector(".error");
    private By loginTitle = By.xpath("//*[@id=\"leftPanel\"]/p");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillUsername(String username) {
        driver.findElement(usernameInput).sendKeys(username);
    }

    public void fillPassword(String password) {
        driver.findElement(passwordInput).sendKeys(password);
    }

    public void submit() {
        driver.findElement(loginBtn).click();
    }

    public String getLoginError() { return driver.findElement(loginError).getText(); }

    public String checkLoginError(){
        return (driver.findElement(loginError).getText());
    }

    public String getLoginTitle() {
        return driver.findElement(loginTitle).getText();
    }
}
