package sitePages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import testData.TestData;

import java.sql.Time;
import java.time.Duration;

public class NewAccountPage {
    private WebDriver driver;
    private TestData testData;

    private By accountTypeInput = By.id("type");
    private By fromAccountIdInput = By.id("fromAccountId");
    private By accountTitle = By.xpath("//*[@id=\"rightPanel\"]/div/div/h1");
    private By accountSubtitle = By.xpath("//*[@id=\"rightPanel\"]/div/div/p[1]");
    private By accountText = By.xpath("//*[@id=\"rightPanel\"]/div/div/p[2]/b");
    private By newAccountCreated = By.id("newAccountId");
    private By openAccountButton = By.cssSelector("[type=submit][value=\"Open New Account\"]");

    public NewAccountPage(WebDriver driver, TestData testData)  {
        this.driver = driver;
        this.testData = testData;
    }

    public String getAccountsTitle() {
        return driver.findElement(accountTitle).getText();
    }

    public WebElement getAccountTitle() {
        return driver.findElement(accountTitle);
    }

    public String getAccountSubtitle() {
        return driver.findElement(accountSubtitle).getText();
    }

    public String getAccountText() {
        return driver.findElement(accountText).getText();
    }

    public String getNewAccountCreated() {
        return driver.findElement(newAccountCreated).getText();
    }

    public void setAccountType(String accountType) {
        Select accountTypes = new Select(driver.findElement(accountTypeInput));
        accountTypes.selectByVisibleText(accountType.toUpperCase());
    }

    public String setFromAccountIdInput() {
        new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.elementToBeClickable(driver.findElement(fromAccountIdInput)));
        Select fromAccountId = new Select(driver.findElement(fromAccountIdInput));
        fromAccountId.selectByVisibleText(testData.bankAccounts.get(0).getAccountID());
        System.out.println("Selected account: "+fromAccountId.getFirstSelectedOption().getAttribute("value"));
        return fromAccountId.getFirstSelectedOption().getAttribute("value");
    }

    public void clickOpenNewAccountButton() {
        WebElement btn = driver.findElement(openAccountButton);
        //btn.sendKeys(Keys.ENTER);
        //new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.elementToBeClickable(btn));
        btn.submit();
    }
}
