package sitePages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import testData.TestData;

public class ApplicationMainPage {
    public WebDriver driver;
    public TestData testData;

    public ApplicationMainPage(WebDriver driver, TestData testData) {
        this.driver = driver;
        this.testData = testData;
    }

    public LoginPage navigateToLoginPage() {
        driver.get("https://parabank.parasoft.com/parabank/index.htm");
        return new LoginPage(driver);
    }

    public RegisterPage navigateToRegisterPage() {
        driver.get("https://parabank.parasoft.com/parabank/index.htm");
        driver.findElement(By.xpath("//*[@id=\"loginPanel\"]/p[2]/a")).click();
        return new RegisterPage(driver);
    }

    public AccountsOverviewPage navigateToAccountsOverviewPage() {
        driver.get("https://parabank.parasoft.com/parabank/overview.htm");
        return new AccountsOverviewPage(driver);
    }

    public NewAccountPage navigateToNewAccountPage() throws InterruptedException {
        driver.get("https://parabank.parasoft.com/parabank/openaccount.htm");
        Thread.sleep(5000);
        return new NewAccountPage(driver,testData);
    }
}
