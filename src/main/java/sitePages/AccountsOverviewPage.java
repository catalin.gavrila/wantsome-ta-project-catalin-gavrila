package sitePages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import testData.BankAccount;

import java.util.ArrayList;
import java.util.List;

public class AccountsOverviewPage {
    private WebDriver driver;

    private By accountsTable = By.id("accountTable");
    private By accountDetails = By.cssSelector("tr[ng-repeat=\"account in accounts\"]");
    private By accountID = By.cssSelector("td a");
    private By accountBalance = By.xpath("td[2]");
    private By accountAvailable = By.xpath("td[2]");

    public AccountsOverviewPage(WebDriver driver) {this.driver = driver;}

    public BankAccount getAccountDetails(WebElement accountDetail) {
        BankAccount account = new BankAccount();
        account.setAccountID(accountDetail.findElement(accountID).getText());
        account.setAccountBalance(accountDetail.findElement(accountBalance).getText());
        account.setAccountAvailable(accountDetail.findElement(accountAvailable).getText());
        return account;
    }

    public List<BankAccount> getBankAccounts() {
        List<BankAccount> bankAccounts = new ArrayList<>();
        for (WebElement bankAccount:driver.findElement(accountsTable).findElements(accountDetails)
             ) {
            bankAccounts.add(getAccountDetails(bankAccount));
        }
        return bankAccounts;
    }

    //public boolean compareBankAccounts(List<BankAccount> src)
}
