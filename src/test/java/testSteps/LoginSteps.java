package testSteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import sitePages.ApplicationMainPage;
import sitePages.LoginPage;

import static utils.Hooks.driver;
import static utils.Hooks.testData;


public class LoginSteps {
    LoginPage loginPage;


    @Given("Open the login page")
    public void openTheLoginPage() {
        loginPage = new ApplicationMainPage(driver,testData).navigateToLoginPage();
    }

    @When("Put the username {string}")
    public void putTheUsername(String username) {
        loginPage.fillUsername(username);
    }

    @And("Put the password {string}")
    public void putThePasswordPassword(String password) {
        loginPage.fillPassword(password);
    }

    @And("Click Submit")
    public void clickSubmit() {
        loginPage.submit();
    }

    @Then("Error message is displayed {string}")
    public void errorMessageIsDisplayed(String errMsg) {
        //System.out.println(loginPage.getLoginError());
        Assert.assertTrue("Eroare incorecta",errMsg.equalsIgnoreCase(loginPage.checkLoginError()));
    }

    @Then("Title after login is {string}")
    public void titleAfterLoginIs(String arg0) {
        System.out.println(loginPage.getLoginTitle());
        Assert.assertTrue("User nu a fost logat",loginPage.getLoginTitle().equalsIgnoreCase(arg0));
    }

    @When("Put the username from registered")
    public void putTheUsernameFromRegistered() {
        loginPage.fillUsername(testData.registeredAccount.getUsername());
    }

    @And("Put the password from registered")
    public void putThePasswordFromRegistered() {
        loginPage.fillPassword(testData.registeredAccount.getPassword());
    }

    @Then("Title after login for registered")
    public void titleAfterLoginForRegistered() {
        System.out.println(loginPage.getLoginTitle());
        Assert.assertTrue("User nu a fost logat",
                loginPage.getLoginTitle().
                        equalsIgnoreCase("Welcome "+testData.registeredAccount.getFirstName()+" "+
                                testData.registeredAccount.getLastName()));
    }
}
