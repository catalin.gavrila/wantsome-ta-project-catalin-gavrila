package testSteps;

import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sitePages.AccountsOverviewPage;
import sitePages.ApplicationMainPage;
import sitePages.RegisterPage;



import java.time.Duration;
import java.util.List;

import static utils.Hooks.driver;
import static utils.Hooks.testData;

public class RegisterSteps {
    RegisterPage registerPage;
    AccountsOverviewPage accountsOverviewPage;

    @Given("Open the registration page")
    public void openTheRegistrationPage() { registerPage = new ApplicationMainPage(driver,testData).navigateToRegisterPage();
    }

    @When("When Put credentials from list")
    public void whenPutCredentialsFromList(List<String> dataTable) {
        registerPage.fillFirstName(dataTable.get(0));
        registerPage.fillLastName(dataTable.get(1));
        registerPage.fillAddress(dataTable.get(2));
        registerPage.fillCity(dataTable.get(3));
        registerPage.fillState(dataTable.get(4));
        registerPage.fillZipCode(dataTable.get(5));
        registerPage.fillPhoneNumber(dataTable.get(6));
        registerPage.fillSsn(dataTable.get(7));
        registerPage.fillUsername(dataTable.get(8));
        registerPage.fillPassword(dataTable.get(9));
        registerPage.fillConfirmPass(dataTable.get(10));
    }

    @DataTableType(replaceWithEmptyString = "[blank]")
    public String listOfStringListsType(String cell) {
        return cell;
    }

    @And("Click Register")
    public void clickRegister() {
        registerPage.clickRegister();
    }

    @Then("User is created with message {string}")
    public void userIsCreatedAndLoggedIn(String message) {
        System.out.println(registerPage.getRegisterSuccessMessage());
        Assert.assertTrue("User nu este creat",registerPage.getRegisterSuccessMessage().equalsIgnoreCase(message));
    }

    @And("Logged in with message {string}")
    public void loggedInWithMessageWelcomeUsername(String message) {
        System.out.println(registerPage.getLoggedInTitleMessage());
        Assert.assertTrue("User nu este creat",registerPage.getLoggedInTitleMessage().equalsIgnoreCase(message));
    }

    @And("Wait for user to be created")
    public void waitForUserToBeCreated() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(registerPage.getLoggedInTitle())));
    }

    @And("Title is {string}")
    public void titleIs(String welcomeMsg) {
        Assert.assertTrue("Mesaj incorect",registerPage.getWelcomeMessage().equalsIgnoreCase(welcomeMsg));
    }

    @Then("Error message for firstName is {string}")
    public void errorMessageForFirstNameIs(String arg0) {
        Assert.assertTrue("Mesaj incorect",registerPage.getFirstNameErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for lastName is {string}")
    public void errorMessageForLastNameIs(String arg0) {
        Assert.assertTrue("Mesaj incorect",registerPage.getLastNameErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for address is {string}")
    public void errorMessageForAddressIs(String arg0) {
        Assert.assertTrue("Mesaj incorect",registerPage.getAddressErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for city is {string}")
    public void errorMessageForCityIs(String arg0) {
        Assert.assertTrue("Mesaj incorect", registerPage.getCityErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for state is {string}")
    public void errorMessageForStateIs(String arg0) {
        Assert.assertTrue("Mesaj incorect", registerPage.getStateErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for zipCode is {string}")
    public void errorMessageForZipCodeIs(String arg0) {
        Assert.assertTrue("Mesaj incorect", registerPage.getZipCodeErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for ssn is {string}")
    public void errorMessageForSsnIs(String arg0) {
        Assert.assertTrue("Mesaj incorect", registerPage.getSsnErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for username is {string}")
    public void errorMessageForUsernameIs(String arg0) {
        Assert.assertTrue("Mesaj incorect", registerPage.getUsernameErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for password is {string}")
    public void errorMessageForPasswordIs(String arg0) {
        Assert.assertTrue("Mesaj incorect", registerPage.getPasswordErrors().equalsIgnoreCase(arg0));
    }

    @And("Error message for confirmPassword is {string}")
    public void errorMessageForConfirmPasswordIs(String arg0) {
        Assert.assertTrue("Mesaj incorect", registerPage.getConfirmPasswordErrors().equalsIgnoreCase(arg0));
    }

    @And("Get account number")
    public void getAccountNumber() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        accountsOverviewPage = new ApplicationMainPage(driver,testData).navigateToAccountsOverviewPage();
        testData.bankAccounts.addAll(accountsOverviewPage.getBankAccounts());
        System.out.println(testData.bankAccounts.toString());
    }

    @And("Save registration details")
    public void saveRegistrationDetails(List<String> dataTable) {
        testData.registeredAccount.setFirstName(dataTable.get(0));
        testData.registeredAccount.setLastName(dataTable.get(1));
        testData.registeredAccount.setAddress(dataTable.get(2));
        testData.registeredAccount.setCity(dataTable.get(3));
        testData.registeredAccount.setState(dataTable.get(4));
        testData.registeredAccount.setZipCode(dataTable.get(5));
        testData.registeredAccount.setPhoneNo(dataTable.get(6));
        testData.registeredAccount.setSsn(dataTable.get(7));
        testData.registeredAccount.setUsername(dataTable.get(8));
        testData.registeredAccount.setPassword(dataTable.get(9));
        System.out.println(testData.registeredAccount.toString());
    }

}
