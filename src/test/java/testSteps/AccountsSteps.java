package testSteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sitePages.AccountsOverviewPage;
import sitePages.ApplicationMainPage;
import sitePages.NewAccountPage;
import testData.BankAccount;

import java.time.Duration;

import static utils.Hooks.driver;
import static utils.Hooks.testData;


public class AccountsSteps {
    AccountsOverviewPage accountsOverviewPage;
    NewAccountPage newAccountPage;

    @Given("Open the new account page")
    public void openTheNewAccountPage() throws InterruptedException {
        //driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        newAccountPage = new ApplicationMainPage(driver,testData).navigateToNewAccountPage();
    }

    @And("Select {string} account")
    public void selectAccount(String arg0) {
        newAccountPage.setAccountType(arg0);
    }

    @And("Select transfer account")
    public void selectTransferAccount() {
        String transferAcctID = newAccountPage.setFromAccountIdInput();
        System.out.println("Transfered from:"+transferAcctID);
        System.out.println(testData.bankAccounts.toString());
        for (BankAccount acct: testData.bankAccounts) {
            if (acct.getAccountID().equalsIgnoreCase(transferAcctID)) {
                Double balance = Double.valueOf(acct.getAccountBalance().substring(1))-100;
                acct.setAccountBalance("$"+balance.toString()+"0");
                Double available = Double.valueOf(acct.getAccountAvailable().substring(1))-100;
                acct.setAccountAvailable("$"+available.toString()+"0");
                System.out.println(acct.toString());
            }
        }
    }

    @And("Click open new account")
    public void clickOpenNewAccount()  throws InterruptedException{
        //driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        newAccountPage.clickOpenNewAccountButton();
        Thread.sleep(2000);
    }

    @Then("Account title is {string}")
    public void accountTitleIs(String arg0){
        //driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

            //Thread.sleep(5000);

        System.out.println(newAccountPage.getAccountsTitle().toString());
        Assert.assertTrue("nu sunt la fel",newAccountPage.getAccountsTitle().equalsIgnoreCase(arg0));
    }

    @And("Subtitle is {string}")
    public void subtitleIs(String arg0) {
        System.out.println(newAccountPage.getAccountSubtitle());
        Assert.assertTrue(newAccountPage.getAccountSubtitle().equalsIgnoreCase(arg0));
    }

    @And("New account text is {string}")
    public void newAccountTextIs(String arg0) {
        System.out.println(newAccountPage.getAccountText().toString());
        Assert.assertTrue(newAccountPage.getAccountText().equalsIgnoreCase(arg0));
    }

    @Given("Open accounts overview page")
    public void openAccountsOverviewPage() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        accountsOverviewPage = new ApplicationMainPage(driver,testData).navigateToAccountsOverviewPage();
    }

    @Then("Check account details")
    public void checkAccountDetails() {
        System.out.println("Conturile bancare");
        System.out.println("Citite: "+accountsOverviewPage.getBankAccounts().toString());
        System.out.println("Salvate: "+testData.bankAccounts.toString());
        Assert.assertTrue("Nu sunt la fel",accountsOverviewPage.getBankAccounts().equals(testData.bankAccounts));
    }

    @And("Save new account details {string}")
    public void saveNewAccountDetails(String arg0) {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccountID(newAccountPage.getNewAccountCreated());
        bankAccount.setAccountType(arg0);
        bankAccount.setAccountAvailable("$100.00");
        bankAccount.setAccountBalance("$100.00");
        testData.bankAccounts.add(bankAccount);
    }
}
