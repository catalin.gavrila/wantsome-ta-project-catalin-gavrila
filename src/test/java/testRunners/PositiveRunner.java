package testRunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        dryRun = false,
        publish = true,
        features = {"src/test/resources/features/01_register.feature",
                "src/test/resources/features/02_login.feature",
                "src/test/resources/features/03_accounts.feature"},
        glue = {"testSteps","utils"},
        tags = "@Positive",
        plugin = {"pretty", "html:target/PositiveTests.html"}
)

public class PositiveRunner {
}
