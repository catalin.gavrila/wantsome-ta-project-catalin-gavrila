package utils;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import testData.TestData;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class Hooks {
    public static WebDriver driver;
    public static TestData testData;

    @BeforeAll
    public static void initTestData() {
        testData = new TestData();
    }

    @Before
    public void setupBefore() {
        System.out.println("Initialized driver-ul in before din Hooks");

        System.setProperty("webdriver.chrome.driver", "./src/test/java/utils/chromedriver");
        System.setProperty("webdriver.chrome.silentOutput", "true");
        //System.out.println(System.getProperty("webdriver.chrome.driver"));
        Map<String, String> mobileEmulation = new HashMap<>();
        //mobileEmulation.put("deviceName", "iPhone 12 Pro");
        mobileEmulation.put("deviceName", "Samsung Galaxy S20 Ultra");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

        driver = new ChromeDriver();
        //driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        //driver.manage().window().maximize();
    }

    @After
    public void tearDownAfter() {
        System.out.println("Inchidem driver-ul");
        driver.quit();
    }
}
