@Login
Feature: Login tests

  #This is the login test with required and extra info
  @Negative
  Scenario Outline: Login errors with required fields in page
    Given Open the login page

    When Put the username "<username>"
    And Put the password "<password>"
    And Click Submit
    Then Error message is displayed "<message>"
    Examples:
      | username | password | message |
      |          | password | Please enter a username and password. |
      | username |          | Please enter a username and password. |
      | 12345678 | 87654321 | The username and password could not be verified.|

  @Negative
  Scenario: Login success
    Given Open the login page

    When Put the username "username"
    And Put the password "password"
    And Click Submit
    Then Title after login is "Welcome firstName lastName"

  @Positive
  Scenario: Login success from register step
    Given Open the login page

    When Put the username from registered
    And Put the password from registered
    And Click Submit
    Then Title after login for registered