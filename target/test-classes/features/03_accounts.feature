@Accounts
Feature: Modify bank accounts for users
  Background:
    Given Open the login page
    And Put the username from registered
    And Put the password from registered
    And Click Submit
    #And Title after login is "Welcome firstName lastName"

  @Positive
  Scenario Outline: Create another bank account
    Given Open the new account page
    And Select "<acctType>" account
    And Select transfer account
    And Click open new account
    Then Account title is "Account Opened!"
    And Subtitle is "Congratulations, your account is now open."
    And New account text is "Your new account number:"
    And Save new account details "<acctType>"
    Examples:
      | acctType |
      | SAVINGS  |
      | CHECKING |


  @Positive
  Scenario: View existing accounts
    Given Open accounts overview page
    Then Check account details