@Register
Feature: Registration tests

  @Positive
  Scenario Outline: Registration success
    Given Open the registration page
    When When Put credentials from list
      | <firstName> |
      | <lastName> |
      | <address> |
      | <city> |
      | <state> |
      | <zipCode> |
      | <phoneNo> |
      | <ssn> |
      | <username> |
      | <password> |
      | <confirmPassword> |

    And Click Register
    And Wait for user to be created
    Then User is created with message "Your account was created successfully. You are now logged in."
    And Logged in with message "Welcome <username>"
    And Title is "Welcome <firstName> <lastName>"
    And Save registration details
      | <firstName> |
      | <lastName> |
      | <address> |
      | <city> |
      | <state> |
      | <zipCode> |
      | <phoneNo> |
      | <ssn> |
      | <username> |
      | <password> |
    And Get account number
    Examples:
      | firstName | lastName | address | city | state | zipCode | phoneNo | ssn | username | password | confirmPassword |
      | catalin | gavrila | address | city | state | zipCode | phoneNo | ssn | username | password | password       |

  @Negative
  Scenario: Registration with existing username
    Given Open the registration page

    When When Put credentials from list
      | firstName |
      | lastName |
      | address |
      | city |
      | state |
      | zipCode |
      | phoneNo |
      | ssn |
      | username |
      | password |
      | password |

    And Click Register
    Then Error message for username is "This username already exists."

  #Scenario testing register error for empty values
  @Negative
  Scenario: Registration with empty fields
    Given Open the registration page

    When When Put credentials from list
      | [blank] |
      | [blank] |
      | [blank] |
      | [blank] |
      | [blank] |
      | [blank] |
      | [blank] |
      | [blank] |
      | [blank] |
      | [blank] |
      | [blank] |

    And Click Register
    Then Error message for firstName is "First name is required."
    And Error message for lastName is "Last name is required."
    And Error message for address is "Address is required."
    And Error message for city is "City is required."
    And Error message for state is "State is required."
    And Error message for zipCode is "Zip Code is required."
    And Error message for ssn is "Social Security Number is required."
    And Error message for username is "Username is required."
    And Error message for password is "Password is required."
    And Error message for confirmPassword is "Password confirmation is required."

    #Scenario testing register error for empty
  @Negative
  Scenario: Registration with un-matching passwords
    Given Open the registration page

    When When Put credentials from list
      | first |
      | last |
      | address |
      | city |
      | state |
      | zip |
      | phoneNo |
      | ssn |
      | username |
      | password |
      | otherPassword |

    And Click Register
    Then Error message for confirmPassword is "Passwords did not match."

